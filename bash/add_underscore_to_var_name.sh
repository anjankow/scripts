#!/bin/bash
if [ -z $1 ] && [ ! -f variables ]; then
	echo "No files with names to be changed!"
	echo "Pass name of such file as argument or use default name 'variables'"
	exit 1
fi

while IFS= read -r name
do
	echo $name
	NEW_NAME=$( echo $name | sed 's/PHSCARCIAPP/PHSCA_RCIAPP/' )
	echo "new name: $NEW_NAME"
	grep -r -l . | grep -v .sh | xargs -I {} sed -i "s/${name}/${NEW_NAME}/g" {}
done < variables

grep -r -l . | xargs -I {} unix2dos.exe {}
