#!/bin/bash
if [ -z $1 ]
then
	echo "Specify lookup pattern"
	exit 1
fi

PATTERN=$1
echo "Adding all files containing pattern $PATTERN"
find | grep ${PATTERN} | xargs -I {} git add {}

