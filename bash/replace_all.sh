#!/bin/bash
if [ -z $2 ]; then
	echo "Usage: replace_all <old_name> <new_name>"
	exit 1
fi

OLD_NAME="$1"
NEW_NAME="$2"

FILES=$(find -type f -regex '\(.*h\|.*c\)')
echo "${FILES}" | xargs sed -i "s/${OLD_NAME}/${NEW_NAME}/g"
echo "${FILES}" | xargs unix2dos
