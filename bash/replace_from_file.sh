#!/bin/bash
if [ ! -z $1 ]; then
	DATA_FILE="$1"
else
	DATA_FILE="replace_pairs.txt"
fi

if [ ! -f ${DATA_FILE} ]; then
	echo "Provide file with replacement data (replace_pairs.txt) in format <old name>@<new name>"
fi

#delete comments
sed -i -e "s/#.*//g" ${DATA_FILE}
for line in $( cat ${DATA_FILE} )
do
	[ "${line}" == "#.*" ] && continue
	OLD_NAME=$( echo ${line} | awk -F@ '{ print $1 }' )
	NEW_NAME=$( echo ${line} | awk -F@ '{ print $2 }' )
	./replace_all.sh ${OLD_NAME} ${NEW_NAME}
done

