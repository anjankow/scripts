import re
import sys


def get_macro_name(line: str):
    macro = line.split(' ')[1].lstrip().rstrip()
    # just in case it was a function
    macro = macro.split('(')[0]
    print(macro)
    return macro


def search_occurences(macro, file_content):
    occurrences = 0
    for l in file_content:
        if macro in l:
            occurrences += 1
    return occurrences


def get_range_to_remove(macro, macro_position, file_content):
    # macro shall be deleted together with doc
    print('Macro to be deleted: ', macro)
    start_position = macro_position
    for start_pos in reversed(range(0, macro_position)):
        if '#define' in file_content[start_pos] or '#include' in file_content[start_pos]:
            break

        if '/**' in file_content[start_pos]:
            start_position = start_pos
            break

    end_position = macro_position
    while file_content[end_position].rstrip().endswith('\\'):
        # if ends with \, it means that the macro continues in the next line
        end_position += 1

    print('Remove from ', start_position, 'to', end_position)
    for idx in range(start_position, end_position + 1):
        print(file_content[idx])

    return (start_pos, end_position + 1)


def delete_ranges(ranges, file_content):
    # remove from the bottom to keep the numbers unchanged
    for r in reversed(ranges):
        del file_content[r[0]: r[1]]


def delete_unnecessary(file_content: list):
    # ranges is a list of ranges to be deleted from file content
    ranges = []
    for position, line in enumerate(file_content):
        if '#define ' in line:
            macro = get_macro_name(line)
            if search_occurences(macro, file_content) < 2:
                ranges.append(get_range_to_remove(macro, position, file_content))

    if len(ranges) > 0:
        delete_ranges(ranges, file_content)


if len(sys.argv) == 1:
    print('Call the script with a file name!')
    exit(1)

for i in range(1, len(sys.argv)):
    file_name = sys.argv[i]
    with open(file_name, 'r') as file:
        file_content = file.readlines()
    delete_unnecessary(file_content)
    print(file_content)
    with open(file_name, 'w') as file:
        file.writelines(file_content)
