import urllib.request
import re
import csv
import tkinter as tk
from tkinter import filedialog
import openpyxl
import sys
import os

MAX_ROW_INDEX = 100
PATTERN = 'prebid'

# choose the file
root = tk.Tk()
root.withdraw()
file_path = filedialog.askopenfilename(title = "Select data file",filetypes = (("XLSX files","*.xlsx"),("all files","*.*")))

if file_path == "":
	exit("File not chosen")

wb_obj = openpyxl.load_workbook(file_path)

# read the active sheet
sheet = wb_obj.active
# 'Site' column number
col_no = -1
row_index = -1

# Find 'Site' cell and save its coordinate
for column in sheet.iter_cols():
	for cell in column:
		if re.search("site", str(cell.value), re.IGNORECASE) != None:
			col_no = cell.column
			row_index = cell.row + 1
			break
	if col_no != -1:
		break

if col_no == -1:
	exit("'Site' cell not found")
else:
	print("Sites read from column number ",col_no)

sites = []
# Site cell found in column col_no
# row_index is index of first site to check
# now collect all the sites to the list
for i in range(row_index,MAX_ROW_INDEX):
	if (sheet.cell(i, col_no).value != None) and (sheet.cell(i, col_no).value != ""):
		sites.append(sheet.cell(i, col_no).value)
	else:
		break

if len(sites) == 0:
	exit("No websites found")

# sites collected, connect and search for pattern
sites_with_pattern = []
failed_sites = []
for site in sites:
	if not site.startswith("http"):
		site = "https://" + site
	print(site)

	try:
		req = urllib.request.Request(site, headers={'User-Agent': 'Mozilla/5.0'})
		response = urllib.request.urlopen(req)
		# check encoding
		encoding = response.headers.get_content_charset()
		if encoding == None:
			encoding = 'utf-8'
		page_source = response.read().decode(encoding)
		matches = re.search(PATTERN, page_source, re.IGNORECASE)
		print(matches)
		if (matches != None):
			# this site contains the pattern in the source code
			sites_with_pattern.append(site)
	except urllib.error.HTTPError as error:
		print("Error while connecting to a site: ",site)
		print(error.reason)
		print(error.code)
		print(error.headers)
		failed_sites.append(site)
	except urllib.error.URLError as error:
		print("Error while connecting to a site:",site)
		print(error.reason)
		failed_sites.append(site)
	except Exception as error:
		print("Error while connecting to a site:",site)
		print(error)
		failed_sites.append(site)

file_name = os.path.basename(file_path)

found_file_name = 'pattern_found__' + file_name + '.txt'
error_file_name = 'failed_sites__' + file_name + '.txt'

with open(found_file_name, 'w') as f:
	f.write('\n'.join(sites_with_pattern))

with open(error_file_name, 'w') as f:
	f.write('\n'.join(failed_sites))

