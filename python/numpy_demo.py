import numpy as np


def dimensions_demo():
    """ DIMENSIONS """
    # create an array with invalid dimensions
    arr_invalid = np.array([[1, 2, 3], [4, 5]])
    print(arr_invalid)

    # create numpy array out of a list
    # check the dimentions
    arr2 = np.array(
        [[1, 2], [3, 4], [5, 6]])

    # # # # # # # # # # # # #
    # [1, 2], [3, 4], [5, 6]]
    #   ==
    # |1|2|
    # |3|4|
    # |5|6|
    # # # # # # # # # # # # #

    print(arr2)
    print(arr2.shape)

    arr3 = np.array(
        [[[1, 2], [3, 4], [5, 6]], [[11, 12], [13, 14], [15, 16]]])

    print(arr3)
    print(arr3.shape)


def size_manipulation_demo():
    """ SIZE MANIPULATION """
    # Create an initial array
    arr = np.arange(6)
    print('Initial array:')
    print(arr)

    # after reshaping the number of params shall remain same
    a1 = arr.reshape((2, 2), order='C')
    print('Reshape with C order:')
    print(a1)
    a2 = arr.reshape((3, 2), order='F')
    print('Reshape with F order:')
    print(a2)

    # resizing can cause losing the elements
    b1 = np.resize(a1, (2, 2))
    print('Resize the a1 array:')
    print(b1)

    # resize to the bigger size - copy directly from the initial array memory,
    # disregarding axes
    b2 = np.resize(b1, (3, 3))
    print('Resize the b1 array:')
    print(b2)


def functions_demo():
    """ NUMPY FUNCTIONS """
    input_arr = np.array([0, np.pi / 6, np.pi / 4, np.pi / 2])
    print(input_arr)
    sin_arr = np.sin(input_arr)
    print(sin_arr)
    print()

    input_arr = [[3, 4], [1, 2], [5, 6]]
    # |3|4|
    # |1|2|
    # |5|6|

    print('Summing up the elements')
    print('Along the axis 0:')
    print(np.sum(input_arr, axis=0))
    print('Along the axis 1:')
    print(np.sum(input_arr, axis=1))
    print()
    print('Max element')
    print('Along the axis 0:')
    print(np.amax(input_arr, axis=0))
    print('Along the axis 1:')
    print(np.amax(input_arr, axis=1))


# dimensions_demo()
# size_manipulation_demo()
# functions_demo()
