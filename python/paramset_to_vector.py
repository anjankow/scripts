import re
file = 'test.cpp'

def change_cpp():
    content = ''
    with open(file, 'r') as f:
        for position, line in enumerate(f):
            if 'makeVector<' in line:

                type = re.search('<(.*)>', line).group(0).replace('<', '').replace('>', '')
                name = line.split('(')[2].split(',')[0]
                line_begin = re.search('.*::ValuesIn\(', line).group(0)
                line_ending = 'UtHelper::makeVectorOfPointers<' + type + '>(' + name + '))'
                line = line_begin + line_ending
                print(line)
            content += line

    with open(file, 'w') as f:
        f.write(content)

def change_h():
    content = ''
    with open(file, 'r') as f:
        for position, line in enumerate(f):
            if '[] = {' in line:
                name = line.split(' ')[1].replace('[]', '')
                type = line.split(' ')[0]
                line = 'auto ' + name + ' = std::vector<' + type + '>({\n'
                print(line)
            content += line

    with open(file, 'w') as f:
        f.write(content)

# change_h()
change_cpp()
