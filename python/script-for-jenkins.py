from turtle import *
import datetime

DEPTH = 11
colors = ['green', 'orange', 'brown', 'black', 'red', 'DarkStateGray1', 'DarkOrchid1']
color1 = 'black'
color2 = 'black'
color3 = 'black'


def tree(length, angle):
    if length < DEPTH:
        return
    else:
        pencolor(color1)
        forward(length)
        right(angle)
        tree(0.8 * length, angle)
        pencolor(color2)
        left(2 * angle)
        tree(0.8 * length, angle)
        right(angle)
        backward(length)
        pencolor(color3)


def draw_fractal():
    pensize(2)
    speed(1000)
    up()
    left(90)
    backward(200.0)
    down()
    tree(120, 230)


epoch = datetime.datetime.now().timestamp()
color1 = colors[int(epoch) % len(colors)]
color2 = colors[int(epoch + 1) % len(colors)]
color3 = colors[int(epoch + 2) % len(colors)]

angle_start = epoch % 360
print('Start angle:', angle_start)

if angle_start < 5 or angle_start > 350:
    exit(2)

if 130 < angle_start < 230:
    exit(1)

draw_fractal()
ts = getscreen()
ts.getcanvas().postscript(file='output.eps')
exit(0)
